<?php
    // Definimos el nombre de las columnas en un array
    $titulos = array("CODIGO", "PRODUCTO", "PRECIO", "PEDIDO MÍNIMO", "CATEGORIA", "ESPECIFICACIONES");

	// Obtenemos conexión con MySQL
    require_once('include/database.php');

    // Cargamos todos los productos de la base de datos
    $query = $mysql->query("
        SELECT PRO_ID, PRO_NOMBRE, PRO_PRECIO, PRO_MIN_UNIDADES, CAT_NOMBRE, PRO_DESCRIPCION
        FROM PRODUCTOS
        JOIN CATEGORIAS ON (PRO_CATEGORIA = CAT_ID)
        ORDER BY PRO_ID ASC
    ");

    // Cabeceras para la descarga del Excel
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");
    header("Content-Disposition: attachment;filename=catalogo.csv");
    header("Content-Transfer-Encoding: binary");

    // Creamos un fichero en el directorio de descarga PHP (php://output)
    $ficheroExcel = fopen("php://output",'w');

    // Añadimos los títulos a las columnas con la función de PHP fputcsv
    fputcsv($ficheroExcel, $titulos, ';');

    // Recorremos los productos y los añadimos al fichero Excel
	while($pro = $query->fetch_object()){
        $linea = array(
            (int)$pro->PRO_ID,
            $pro->PRO_NOMBRE,
            (float)number_format($pro->PRO_PRECIO,2),
            (int)$pro->PRO_MIN_UNIDADES,
            $pro->CAT_NOMBRE,
            $pro->PRO_DESCRIPCION
        );
        fputcsv($ficheroExcel, $linea, ';');
    }

    // Cerramos el fichero Excel
    fclose($ficheroExcel);

    // Cerramos conexión con MySQL
    $mysql->close();

    // Finalizamos la ejecución del script
    exit;
?>
