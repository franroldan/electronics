<?php
	// Cargamos la cabecera de la web
	date_default_timezone_set('Etc/UTC');
	require_once("include/header.php");

	// Proceso de envío de un email desde el formulario de contacto
	if(isset($_POST['enviar'])){
		// Cargamos la librería PHPMailer
		require_once('include/PHPMailer/class.phpmailer.php');
		// Datos del email
		$nombre = trim($_POST['nombre']);
		$email = trim($_POST['email']);
		$asunto = trim($_POST['asunto']);
		$para = 'enriqueproyectoasir@gmail.com';
		// Envío
		$mail = new PHPMailer(true);
		$mail->IsSMTP();
		$mail->SMTPDebug  = 1;                     
		$mail->SMTPAuth   = true;   
		$mail->SMTPSecure = 'tls';               
		$mail->Host       = 'smtp.gmail.com'; 
		$mail->Port       = 587;  
		$mail->Username   = $para;
		$mail->Password   = 'correo.123';
		$mail->SetFrom($email,'Cliente');
		$mail->AddAddress($para, 'Electronics');
		$mail->Subject = $asunto;
		$mensaje = "<h3>MENSAJE RECIBIDO DESDE LA WEB<\h3>";
		$mensaje .= '<ul><li>Nombre: '.$nombre."<\li>";
		$mensaje .= '<li>Email: '.$email."<\li>";
		$mensaje .= '<p>'.trim($_POST['mensaje']).'</p>';
		$mail->isHTML(true);
		$mail->MsgHTML($mensaje);
		if(!$mail->Send()){
			?><script>alert('Error: el mensaje no ha podido ser enviado por problemas técnicos. Si el prolema persiste contacte con el administrador. Sentimos las molestias.');</script><?php
		}else{
			?><script>alert('Info: el email ha sido enviado correctamente. Le responderemos en cuanto nos sea posible. Gracias.');</script><?php
		}
	}
?>
<div class="map">
	<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBDoiWR_p8YBdD4jwKm85V_FKF64NtAEXI&q=Calle+de+Rodriguez+San+Pedro+2,Madrid+Spain"></iframe>
</div>
<div class="container">
	<div class="contact">
		<h2>Conctacto</h2>
		<div class="contact-in">
			<div class=" col-md-3 contact-right">
		     	<h5>Dirección y datos de contacto</h5>
		    	<p>Calle de Rodríguez San Pedro número 2</p>
		   		<p>28015, Madrid, España (ES)</p>
				<p>Teléfonop: (+34) 951 455 312</p>
		   		<p>WhatsApp: (+34) 626 776 404</p>
		   		<p>Fax: 345 33 30 8</p>
		 	 	<p>Email: <a href="mailto:info@electronics.com">info@electronics.com</a></p>
		   		<p>Síguenos en <a href="https://www.facebook.com/">Facebook</a> y <a href="https://twitter.com/">Twitter</a></p>
		    </div>
			<div class="col-md-9 contact-left">
				<form action="contact.php" method="POST">
			    	<div>
				    	<span>Nombre</span>
				    	<input name="nombre" type="text" class="textbox" placeholder="Nombre..." required />
				    </div>
				    <div>
				    	<span>E-Mail</span>
				    	<input name="email" type="text" class="textbox" placeholder="Email..." required />
				    </div>
					<div>
				    	<span>Asunto</span>
				    	<input name="asunto" type="text" class="textbox" placeholder="Asunto..." required />
				    </div>
				    <div>
				    	<span>Mensaje</span>
				    	<textarea name="mensaje" required >Mensaje...</textarea>
				    </div>
				   	<div>
				   		<input type="submit" value=" Enviar " name="enviar" />
				  	</div>
		    	</form>
		  	</div>
			<div class="clearfix"></div>
	 	</div>
	</div>
</div>
<?php
	// Incluimos el pie de página
	require_once("include/footer.php");
?>
