<?php
	// Cargamos los productos más recientes (hasta 8)
	// que contenga allguna imágen
	$query = $mysql->query("
		SELECT pro.*, (
			SELECT img.IMG_URL
			FROM IMAGENES_PRODUCTOS ip
			JOIN IMAGENES img ON (img.IMG_ID = ip.IP_IMAGEN)
			WHERE ip.IP_PRODUCTO = pro.PRO_ID
			ORDER BY RAND()
			LIMIT 1
		) AS PRO_URL
		FROM PRODUCTOS pro
		ORDER BY PRO_FECHA DESC
		LIMIT 8
	");
?>
<!-- Javascript -->
<script type="text/javascript">
	$(document).ready(function() {
		$("#owl-demo").owlCarousel({
			items : 4,
			lazyLoad : true,
			autoPlay : true,
			pagination : true,
		});
	});
</script>
<!-- Recent -->
<div class="content-top">
	<h2 class="new">Productos más recientes</h2>
	<div class="pink">
		<div id="owl-demo" class="owl-carousel text-center">
			<?php while($pro = $query->fetch_object()){ ?>
			<div class="item">
				<div class=" box-in">
					<div class="grid_box">
						<a href="detail.php?product=<?=$pro->PRO_ID?>">
							<img src="images/<?=$pro->PRO_URL?>" class="img-responsive" alt="">
							<div class="zoom-icon">
								<ul class="in-by">
									<li><?=utf8_encode($categorias[$pro->PRO_CATEGORIA])?></li>
								</ul>
								<ul class="in-by-color">
									<li><?=date('d/m/Y H:i',strtotime($pro->PRO_FECHA))?></li>
								</ul>
							</div>
						</a>
					</div>
					<div class="grid_1 simpleCart_shelfItem">
						<a href="detail.php?product=<?=$pro->PRO_ID?>" class="cup item_add">
							<span class="item_price"><?=number_format($pro->PRO_PRECIO,2)?> &euro;</span>
						</a>
					</div>
				</div>
			</div>
			<?php } ?>
			<div class="clearfix"> </div>
		</div>
	</div>
 </div>
