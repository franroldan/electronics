<?php
	// Iniciamos el control de session_id
	session_start();

	// COnfiguración de PHP - Mostrar los errores
	error_reporting(E_ALL);

	// Incluimos las funciones de base de datos
	require_once("database.php");

	// Cargamos el listado de cartegorías
	$categorias = array();
	$query = $mysql->query("SELECT * FROM CATEGORIAS ORDER BY RAND()");
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Meta -->
		<title>Electronics - Distribuidor mayorista de artículos de electrónica</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="electrónica, distribuidor, informática, mayorista, mejores precios, venta online, Madrid" />
		<meta name="description" content="Distribuimos al por mayor artículos de informática y electrónica a toda España." />
		<!-- CSS -->
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/owl.carousel.css" rel="stylesheet" media="all" />
		<link href="include/DataTables/media/css/jquery.dataTables.css" rel="stylesheet" media="all" />
		<link href='//fonts.googleapis.com/css?family=Amaranth:400,700' rel='stylesheet' type='text/css' />
		<link href="images/fav.ico" rel="shortcut icon" type="image/x-icon">
		<link rel="icon" href="images/fav.ico" type="image/x-icon">
		<!-- JavaScript -->
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/move-top.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>
		<script type="text/javascript" src="js/megamenu.js"></script>
		<script type="text/javascript" src="js/jquery.wmuSlider.js"></script>
		<script type="text/javascript" src="js/owl.carousel.js"></script>
		<script type="text/javascript" src="js/imagezoom.js"></script>
		<script type="text/javascript" src="js/jquery.flexslider.js"></script>
		<script type="text/javascript" src="include/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
		<script type="text/javascript">
			addEventListener("load", function(){
				setTimeout(hideURLbar, 0);
			}, false);

			function hideURLbar(){
				window.scrollTo(0,1);
			}

			function googleTranslateElementInit() {
		  		new google.translate.TranslateElement({pageLanguage: 'es', includedLanguages: 'de,en,fr', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
			}

			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
				$(".megamenu").megamenu();
				$().UItoTop({ easingType: 'easeOutQuart' });
			});
		</script>
	</head>
<body>
<!-- Content Header -->
<div class="header" >
	<div class="top-header" >
		<div class="container">
		<div class="top-head" >
			<!-- Social Network -->
			<div class="header-para">
				<ul class="social-in">
					<li>
						<a href="https://twitter.com/" target="_blank"><i></i></a>
					</li>
					<li>
						<a href="https://www.facebook.com/" target="_blank"><i class="ic"></i></a>
					</li>
					<li>
						<a href="https://es.wikipedia.org/wiki/RSS" target="_blank"><i class="ic1"></i></a>
					</li>
				</ul>
			</div>
			<div class="search-top">
				<div class="world" id="google_translate_element"></div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<div class="header-top">
	<div class="container">
		<div class="head-top">
			<div class="logo">
				<a href="home.php"><img src="images/logo.png" alt="" ></a>
			</div>
			<div class="top-nav">
				<ul class="megamenu skyblue">
					<?php while($cat = $query->fetch_object()){
						$categorias[$cat->CAT_ID] = $cat->CAT_NOMBRE; ?>
					<li><a class="headerLink" href="list.php?category=<?=$cat->CAT_ID?>"><?=utf8_encode($cat->CAT_NOMBRE)?></a></li>
					<?php } ?>
					<li class="headerLink"><a href="excel.php">Descargar catálogo en Excel</a></li>
					<li class="headerLink"><a href="contact.php">Contacto</a></li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
