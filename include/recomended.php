<?php
    // Cargamos hasta 3 productos de forma aleatoria
    $query = $mysql->query("
        SELECT pro.*, (
            SELECT img.IMG_URL
            FROM IMAGENES_PRODUCTOS ip
            JOIN IMAGENES img ON (img.IMG_ID = ip.IP_IMAGEN)
            WHERE ip.IP_PRODUCTO = pro.PRO_ID
            ORDER BY RAND()
			LIMIT 1
        ) AS PRO_URL
        FROM PRODUCTOS pro
        ORDER BY RAND()
        LIMIT 3
    ");
?>
<!-- Recomended -->
<div class="content-middle">
    <h2 class="middle">Productos recomendados</h2>
    <div class="col-best">
        <?php while($pro = $query->fetch_object()){ ?>
        <div class="col-md-4">
            <a href="detail.php?product=<?=$pro->PRO_ID?>">
                <div class="col-in">
                    <div class="col-in-left">
                        <img src="images/<?=$pro->PRO_URL?>" class="img-responsive" alt="">
                        <br/>
                    </div>
                </a>
                <div class="col-in-right grid_1 simpleCart_shelfItem" style="width:100%;text-align:center;">
                    <h5><?=utf8_encode($pro->PRO_NOMBRE)?></h5>
                    <br/>
                    <a href="detail.php?product=<?=$pro->PRO_ID?>" class="item_add">
    					<span class="white item_price"><?=number_format($pro->PRO_PRECIO,2)?> &euro;</span>
    				</a>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <?php } ?>
        <div class="clearfix"> </div>
    </div>
</div>
