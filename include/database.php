<?php
	// Variables necesarias para la conexión con MySQL
    $bd_location = $_ENV['OPENSHIFT_MYSQL_DB_HOST'];
	$bd_user = $_ENV['OPENSHIFT_MYSQL_DB_USERNAME'];
	$bd_pass = $_ENV['OPENSHIFT_MYSQL_DB_PASSWORD'];
	$bd_name = $_ENV['OPENSHIFT_APP_NAME'];
	$bd_port = $_ENV['OPENSHIFT_MYSQL_DB_PORT'];

    // Conexión con MySQL
    $mysql = new mysqli($bd_location, $bd_user, $bd_pass, $bd_name, $bd_port);
	if($mysql->connect_errno){
		die('MySQL error: ('.$mysql->connect_errno.') '.$mysql->connect_error);
	}
?>
