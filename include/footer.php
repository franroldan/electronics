        <!-- Footer -->
        <div class="footer">
            <div class="container">
                <div class="col-md-2 footer-left">
                    <a href="index.html"><img src="images/logo.png" alt=""></a>
                </div>
                <div class="col-md-3 footer-middle">
                    <p class="footer-class">Electronics © 2015 - Todos los derechos reservados</p>
                </div>
                <div class="col-md-3 footer-left-in">
                    <div class="clearfix"> </div>
                </div>
                <div class="col-md-4 footer-right">
                    <h5>ACEPTAMOS PAGO CON TARJETA</h5>
                    <ul>
                        <li><a><i></i></a></li>
                        <li><a><i class="we"></i></a></li>
                        <li><a><i class="we-in"></i></a></li>
                        <li><a><i class="we-at"></i></a></li>
                        <li><a><i class="we-at-at"></i></a></li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
            <a href="#" id="toTop" style="display: block;">
                <span id="toTopHover" style="opacity: 1;"></span>
            </a>
        </div>
    </body>
</html>
<?php
    // Cerramos la conexión con MySQL
    $mysql->close();
?>
