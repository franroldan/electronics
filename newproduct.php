<?php
    // Cargamos la cabecera de la web
	require_once("include/header.php");

    // Si no somos administradores redirigimos al home
    if(!isset($_SESSION["administrador"])){
        header("Location: home.php");
        ?><script>location.href="home.php";</script><?php
        exit;
	}

	// Comprobamos subida de un nuevo producto
	if(isset($_POST['subir'])){
		// Obtenemos los datos del producto
		$nombre = trim(utf8_decode($_POST['nombre']));
		$descripcion = trim(utf8_decode($_POST['descripcion']));
		$precio = (double)$_POST['precio'];
		$unidades = (int)$_POST['unidades'];
		$categoria = (int)$_POST['categoria'];
		// Subimos el producto a la base de datos
		$mysql->query("INSERT INTO PRODUCTOS (PRO_NOMBRE, PRO_DESCRIPCION, PRO_PRECIO, PRO_MIN_UNIDADES, PRO_CATEGORIA)
			VALUES ('$nombre','$descripcion',$precio,$unidades,$categoria)");
		$pro = $mysql->insert_id;
		// Subimos las imágenes
		for($i=0;$i<count($_FILES['imagenes']["name"]);$i++){
			subeImagen($i,$pro);
		}
		// Mensaje de confirmación
		?><script>alert("Info: producto subido correctamente");</script><?php
	}

	function subeImagen($_i,$_producto){
		global $mysql;
		$extension = explode('.',$_FILES["imagenes"]["name"][$_i]);
		$fichero = time().'.'.$extension[count($extension)-1];
		move_uploaded_file($_FILES["imagenes"]["tmp_name"][$_i],"images/".$fichero);
		$mysql->query("INSERT INTO IMAGENES (IMG_URL) VALUES ('$fichero')");
		$mysql->query("INSERT INTO IMAGENES_PRODUCTOS (IP_IMAGEN,IP_PRODUCTO) VALUES ({$mysql->insert_id},$_producto)");
	}
?>
<!-- Javascript -->
<script type="text/javascript">
	function nuevaImagen(){
		console.log("nuevaImagen");
		$("#listadoFotos").append('<p><input type="file" name="imagenes[]" required /></p>');
	}
</script>
<!-- Content -->
<div class="content">
	<div class="container">
		<h2 style="text-align:center;margin-top:25px;" class="col-md-12" enctype="multipart/form-data">
			Nuevo producto
		</h2>
		<div class="col-md-12"><br/></div>
		<form action="newproduct.php" method="POST" class="col-md-12" style="text-align:center;" enctype="multipart/form-data">
			<div class="col-md-4">
				Nombre: <input type="text" name="nombre" placeholder="Nombre del producto..." size="40" required />
			</div>
			<div class="col-md-4">
				Categoría: <select name="categoria">
				<?php foreach($categorias as $c_id => $c_nombre){ ?>
					<option value="<?=$c_id?>"><?=utf8_encode($c_nombre)?></option>
				<?php } ?>
				</select>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-12"><br/></div>
			<div class="col-md-4">
				Precio: <input type="number" min="0.01" step="any" name="precio" placeholder="Precio..." required />
			</div>
			<div class="col-md-4">
				Min. unidades: <input type="number" min="1" name="unidades" placeholder="Unidades..." required />
			</div>
			<div class="col-md-4">
				<input type="submit" name="subir" value="Subir nuevo producto" />
			</div>
			<div class="col-md-12"><br/></div>
			<div class="col-md-12">
				Descripción: <br/><textarea name="descripcion" rows="9" style="width:100%;" required></textarea>
			</div>
			<div class="col-md-12"><br/></div>
			<div class="col-md-12" id="listadoFotos">
				<p><input type="file" name="imagenes[]" required /></p>
			</div>
			<div class="col-md-12">
				<a onclick="nuevaImagen();"> + Añadir más imágenes</a>
				<br/><br/>
				<a href="admin.php">Volver al menú de Administrador</a>
			</div>
		</form>
        <div class="bottom-content">
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<?php
	// Incluimos el pie de página
	require_once("include/footer.php");
?>
