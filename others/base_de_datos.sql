SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

CREATE DATABASE IF NOT EXISTS `electronics` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `electronics`;

CREATE TABLE IF NOT EXISTS `ADMINISTRADORES` (
  `ADM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ADM_CLAVE` varchar(300) NOT NULL,
  PRIMARY KEY (`ADM_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

INSERT INTO `ADMINISTRADORES` (`ADM_ID`, `ADM_CLAVE`) VALUES
(1, '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1');

CREATE TABLE IF NOT EXISTS `CATEGORIAS` (
  `CAT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CAT_NOMBRE` varchar(100) NOT NULL,
  PRIMARY KEY (`CAT_ID`),
  UNIQUE KEY `CAT_NOMBRE` (`CAT_NOMBRE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

INSERT INTO `CATEGORIAS` (`CAT_ID`, `CAT_NOMBRE`) VALUES
(6, 'Dispositivos de Almacenamiento'),
(1, 'Memorias RAM'),
(5, 'Monitores'),
(2, 'Placas Base'),
(4, 'Tarjetas de Sonido'),
(3, 'Tarjetas Gráficas');

CREATE TABLE IF NOT EXISTS `IMAGENES` (
  `IMG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `IMG_URL` varchar(200) NOT NULL,
  PRIMARY KEY (`IMG_ID`),
  UNIQUE KEY `IMG_URL` (`IMG_URL`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

INSERT INTO `IMAGENES` (`IMG_ID`, `IMG_URL`) VALUES
(2, '1449497852.png'),
(3, '1449498136.png'),
(4, '1449498345.jpg'),
(5, '1449498495.jpg'),
(6, '1449498623.png'),
(7, '1449498958.jpg'),
(8, '1449499100.jpg'),
(9, '1449499391.jpg'),
(10, '1449499594.jpg'),
(11, '1449499873.jpg'),
(12, '1449602697.jpg');

CREATE TABLE IF NOT EXISTS `IMAGENES_PRODUCTOS` (
  `IP_IMAGEN` int(11) NOT NULL,
  `IP_PRODUCTO` int(11) NOT NULL,
  KEY `IP_PRODUCTO` (`IP_PRODUCTO`),
  KEY `IP_IMAGEN` (`IP_IMAGEN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `IMAGENES_PRODUCTOS` (`IP_IMAGEN`, `IP_PRODUCTO`) VALUES
(2, 1),
(3, 2),
(4, 3),
(5, 4),
(6, 5),
(7, 6),
(8, 7),
(9, 8),
(10, 9),
(11, 10),
(12, 11);

CREATE TABLE IF NOT EXISTS `PRODUCTOS` (
  `PRO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRO_NOMBRE` varchar(100) NOT NULL,
  `PRO_PRECIO` double NOT NULL,
  `PRO_MIN_UNIDADES` int(10) unsigned NOT NULL,
  `PRO_DESCRIPCION` text NOT NULL,
  `PRO_CATEGORIA` int(11) NOT NULL,
  `PRO_FECHA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PRO_ID`),
  KEY `PRO_CATEGORIA` (`PRO_CATEGORIA`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

INSERT INTO `PRODUCTOS` (`PRO_ID`, `PRO_NOMBRE`, `PRO_PRECIO`, `PRO_MIN_UNIDADES`, `PRO_DESCRIPCION`, `PRO_CATEGORIA`, `PRO_FECHA`) VALUES
(1, 'samsung 850 evo (500GB)', 80, 20, 'Arquitectura 3D V-NAND:\r\nLa exclusiva e innovadora arquitectura Samsung 3D V-NAND supone una revolución en el mundo de las memorias flash en términos de densidad, rendimiento y resistencia. Las memorias 3D V-NAND se fabrican apilando verticalmente 32 capas de celdas. Así, se consigue mayor densidad y rendimiento con un menor consumo energético.', 6, '2015-12-07 09:17:32'),
(2, 'Asus 28" PB287Q', 400, 15, '', 5, '2015-12-07 09:22:16'),
(3, 'Kingston hyperx fury 8GB', 50, 20, 'Máxima potencia y rendimiento asegurado.\r\n\r\nEspecificaciones\r\n\r\nMemoria interna 8GB\r\nTipo de memoria interna DDR3\r\nVelocidad de memoria del reloj* 1600 MHz\r\nComponente para* PC/server\r\nForma de factor de memoria* 240-pin DIMM\r\nMemory layout (modules x size) 1 x 8 GB\r\nLatencia CAS * 10\r\nPC3-12800\r\nVoltaje de memoria * 1.5 V\r\nMemoria sin buffer\r\nColor del producto Azul', 1, '2015-12-07 09:25:45'),
(4, 'asus p8z77', 70, 15, 'Rendimiento gráfico superior para Z77\r\n\r\nDIGI+ VRM ? Control de alimentación digital\r\nUSB3.0 Boost ? Transferencias de datos mucho más rápidas\r\nNetwork iControl<7li>\r\nLucidLogix® Virtu Universal MVP?\r\nGPU Boost', 2, '2015-12-07 09:28:15'),
(5, 'MSI GTX970 GAMING 4GDDR5', 150, 20, 'Unidad de procesamiento gráfico NVIDIA Geforce GTX 970\r\nInterfaz PCI Express x16 3.0\r\nVelocidad del reloj (MHz)\r\n1140MHz Core (Boost Reloj: 1279MHz) (Modo OC)\r\n1114MHz Core (Boost Reloj: 1253MHz) (modo de juego)\r\n1051MHz Core (Boost Reloj: 1178MHz) (modo silencioso)\r\nTamaño de la memoria (MB) 4096\r\nTipo de memoria GDDR5\r\nInterfaz de memoria 256 bits\r\nVelocidad de memoria del reloj (MHz) 7010\r\nConectores DVI 2 (dual-link DVI-I, Dual-link DVI-D) Resolución máxima: 2048x1536 @ 60 Hz.\r\nConectores HDMI 1 (versión 1.4a/2.0) Resolución máxima: 4096x2160 @ 24 Hz\r\nDisplayPort 1 (versión 1.2) Resolución máxima: 4096x2160 @ 60 Hz\r\nMuestra Máximo 4\r\nSoporte HDCP\r\nVelocidad RAMDAC (MHz) 400\r\nSoporte DirectX Version 12\r\nSoporte OpenGL 4.4\r\nTecnología Multi-GPU SLI de 3 vías\r\nTarjeta Dimensión (mm) 269 ??x 141 x 35\r\nTarjeta de Peso (g) 814\r\nConsumo de energía (W) 148\r\nPSU Recomendado (W) 500\r\nConectores de alimentación 1x 8-pin, 1x 6-pin', 3, '2015-12-07 09:30:23'),
(6, 'Crucial ballistic sport 8GB', 60, 20, 'Configuración de módulos: 512M x 64\r\nDiseño de memoria (módulos x tamaño): 2 x 4 GB\r\nECC: No\r\nForma de factor de memoria: 240-pin DIMM\r\nLatencia CAS: 8\r\nMemoria interna: 8 GB\r\nMemoria sin buffer: Si\r\nTipo de embalaje: DIMM\r\nTipo de memoria: PC3-12800\r\nTipo de memoria interna: DDR3\r\nVelocidad de memoria del reloj: 1600 MHz\r\nVoltaje de memoria: 1,5V', 1, '2015-12-07 09:35:58'),
(7, 'Corsair 8GB DDR3 1600MHz VENGEANCE', 70, 15, 'Los módulos de memoria Corsair Vengeance Pro Series están pensados para los overclockers en las últimas plataformas de 3ª y 4ª generación de Intel Core, con IC y PCB de ocho capas especialmente diseñadas para el rendimiento.\r\n\r\nLos disipadores de calor de aluminio proporcionan una gestión de temperatura superior y tienen el aspecto agresivo que busca en su plataforma de juegos. Los perfiles XMP 1.3 permiten un overclocking automático y fiable.\r\n\r\nEstán disponibles en varios colores que le permiten coordinar su memoria Vengeance Pro con el esquema de color del sistema. Además, gracias a su precio muy atractivo, dispondrá de un presupuesto más holgado para ampliar el sistema.', 1, '2015-12-07 09:38:20'),
(8, 'G.Skill Ripjaws DDR4 2400MHz 16GB Kit', 80, 20, 'Series: Ripjaws 4\r\nMemory Type: DDR4\r\nCapacity: 16GB (4GBx4)\r\nMulti-Channel Kit: Quad Channel Kit\r\nTested Speed: 2400MHz\r\nTested Latency: 15-15-15-35-2N\r\nTested Voltage: 1.20v\r\nRegistered/unbuffered: Unbuffered\r\nError Checking: Non-ECC\r\nSPD Speed: 2133MHz\r\nSPD Voltage: 1.20v\r\nFan Included: No\r\nHeight: 40 mm / 1.58 inch\r\nWarranty: Limited Lifetime\r\nFeatures: Intel XMP 2.0 (Extreme Memory Profile) Ready', 1, '2015-12-07 09:43:11'),
(9, 'Kingston HyperX 240GB', 50, 10, 'La memoria HyperX FURY es plug and play (PnP) lo que permite acelerar automáticamente la velocidad del sistema sin necesidad de modificaciones manuales en la BIOS. El nuevo diseño del disipador viene en cuatro colores (azul, negro, rojo y blanco) e incluye el PCB en negro. Todo ello para permitir a los jugadores, modders e integradores de sistemas disponer del color que vaya más acorde con sus sistema. HyperX FURY esta disponible en las frecuencias 1333MHz, 1600MHz y 1866MHz.\r\nA la gama HyperX FURY pronto se le unirá HyperX SSD FURY, diseñado también para los jugadores ocasionales y entusiastas que quieran acelerar sus videojuegos. El nuevo HyperX SSD FURY permitirá a los PCs y a las aplicaciones una carga más rápida y una mejora de la capacidad de respuesta global del sistema.\r\n?Estamos muy contentos de poder ofrecer nuestra nueva incorporación a la familia HyperX DRAM a los entusiastas y aficionados que desean maximizar su experiencia de usuario en videojuegos", comenta Jordi García, responsable de Kingston Technology Iberia. "Sin duda HyperX FURY es un gran producto para los que buscan actualizar su sistema de gaming a un precio muy asequible?.\r\nHyperX es la división de productos de alto rendimiento de Kingston Technology que abarca memorias de alta velocidad DDR3, SSDs, unidades flash USB y auriculares. Dirigido a jugadores, overclockers y entusiastas, HyperX es conocido en todo el mundo por su calidad, rendimiento e innovación. HyperX está comprometido con los eSports, patrocina más de 20 equipos en todo el mundo y es el principal patrocinador del Intel Extreme Masters. HyperX tiene presencia en gran cantidad de eventos, incluyendo Brasil Game Show, China Joy, DreamHack, gamescom y PAX.', 6, '2015-12-07 09:46:34'),
(10, 'Monitor lcd dell 23"', 80, 20, 'Disfrute desde el primer instante de una experiencia de sensacional precisión del color gracias al calibrado de color de fábrica. Acomódese a los estándares de diseño profesionales con el 99 % de la gama de colores de Adobe RGB y el 100 % de sRGB a un valor Delta-E inferior a 2.Modifique la tonalidad según sus preferencias de saturación o tono con Dell CustomColour. O elija la posibilidad de calibrar el monitor aún más con el colorímetro X-rite i1Display Pro opcional, que le permite acceder a una LUT (tabla de consulta) de 14 bits a través del software de la solución de calibración de color para Dell UltraSharp.', 5, '2015-12-07 09:51:12'),
(11, 'MSI Z97A', 100, 20, 'La GAMING Z97A 6 se añade a la serie con el conector nuevo USB Tipo-C, también entra la gama amarilla y negra de la serie OC con placas base X99A XPOWER CA y X99A MPOWER. La primera y única placa base totalmente en blanco y negro, Krait, también se actualiza a USB 3.1 con la X99A SLI Krait Edition. En la serie Classic, MSI presenta el USB 3.1 destacando la X99A SLI PLUS y X99A RAIDER. Por último, pero no menos importante, MSI está introduciendo la primer placa madre para AMD con USB 3.1 con la GAMING 990FXA y 970A SLI Krait Edition.', 2, '2015-12-08 14:24:57');


ALTER TABLE `IMAGENES_PRODUCTOS`
  ADD CONSTRAINT `IMAGENES_PRODUCTOS_ibfk_1` FOREIGN KEY (`IP_IMAGEN`) REFERENCES `IMAGENES` (`IMG_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `IMAGENES_PRODUCTOS_ibfk_2` FOREIGN KEY (`IP_PRODUCTO`) REFERENCES `PRODUCTOS` (`PRO_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `PRODUCTOS`
  ADD CONSTRAINT `fk_pro_cat` FOREIGN KEY (`PRO_CATEGORIA`) REFERENCES `CATEGORIAS` (`CAT_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
