<?php
	// Cargamos la cabecera de la web
	require_once("include/header.php");

	// Cargamos el producto
	$producto = null;
	if(isset($_GET['product'])){
		$query = $mysql->query("SELECT * FROM PRODUCTOS WHERE PRO_ID = ".((int)$_GET['product']));
		if($pro = $query->fetch_object()){
			$producto = $pro;
			$imagenes = array();
			$query = $mysql->query("
				SELECT DISTINCT(IMG_URL) FROM IMAGENES
				JOIN IMAGENES_PRODUCTOS ON (IP_IMAGEN = IMG_ID)
				WHERE IP_PRODUCTO = ".$pro->PRO_ID
			);
			while($img = $query->fetch_object()){
				array_push($imagenes,$img->IMG_URL);
			}
		}
	}
	if(is_null($producto) || empty($imagenes)){
		header("Location: home.php");
        ?><script>location.href="home.php";</script><?php
        exit;
	}
?>
<!-- CSS -->
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<!-- JavaScript -->
<script type="text/javascript">
	$(window).load(function() {
		$('.flexslider').flexslider({
			animation: "slide",
			controlNav: "thumbnails"
		});
	});
</script>
<div class="content">
	<div class="container">
		<div class="single">
			<div class="col-md-12 top-in-single">
				<div class="col-md-5 single-top">
					<div class="flexslider">
						<ul class="slides">
							<?php foreach($imagenes as $img){ ?>
							<li data-thumb="images/<?=$img?>">
								<div class="thumb-image">
									<img src="images/<?=$img?>" data-imagezoom="true" class="img-responsive">
								</div>
							</li>
							<?php } ?>
					  	</ul>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-7 single-top-in">
					<div class="single-para">
						<h4><?=utf8_encode($producto->PRO_NOMBRE)?></h4>
						<p><?=utf8_encode($producto->PRO_DESCRIPCION)?></p>
						<br/>
						<label class="add-to"><?=number_format($producto->PRO_PRECIO,2)?> &euro;</label>
						<div class="available">
							<h6><?=utf8_encode($categorias[$producto->PRO_CATEGORIA])?></h6>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<!-- The Best -->
		<?php
			// Incluimos el plan de empresa
			include("include/objetive.html");
	 	?>
		<br/>
	</div>
</div>
<?php
	// Incluimos el pie de página
	require_once("include/footer.php");
?>
