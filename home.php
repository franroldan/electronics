<?php
	// Cargamos la cabecera de la web
	require_once("include/header.php");

	// Incluimos el banner de ofertas
	include("include/banner.html");
?>
<!-- Content -->
<div class="content">
	<div class="container">
		<!-- Recent -->
		<?php
			// Incluimos productos recientes
			include("include/recent.php");
		?>
		<!-- Recomended -->
		<?php
			// Incluimos productos más vendidos
			include("include/recomended.php");
		?>
		<!-- The Best -->
		<?php
			// Incluimos el plan de empresa
			include("include/objetive.html");
	 	?>
		<div class="bottom-content">
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<?php
	// Incluimos el pie de página
	require_once("include/footer.php");
?>
