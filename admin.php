<?php
    // Cargamos la cabecera de la web
	require_once("include/header.php");

    // Comprobamos si nos envían un acceso de administrador
    $error = false;
    if(!empty($_POST["clave"])){
        $clave = sha1(md5(trim($_POST["clave"])));
        $queryAcceso = $mysql->query("
            SELECT ADM_ID FROM ADMINISTRADORES
            WHERE ADM_CLAVE LIKE '$clave'
        ");
        if($queryAcceso->num_rows>0){
            $_SESSION["administrador"] = true;
        }else{
            $error = true;
        }
    }

	// Eliminación de un producto
	if(isset($_POST['eliminar'])){
		$mysql->query("DELETE FROM PRODUCTOS WHERE PRO_ID = ".((int)$_POST['id']));
		?><script>alert("Info: el producto ha sido eliminado");</script><?php
	}

	// Edición de un producto
	if(isset($_POST['editar'])){
		$mysql->query("UPDATE PRODUCTOS SET PRO_PRECIO = ".((double)$_POST['precio']).",
			PRO_MIN_UNIDADES = ".((int)$_POST['unidades']));
		?><script>alert("Info: el producto ha sido editado");</script><?php
	}

	// Cargamos el listado completo de productos
    $query = $mysql->query("SELECT * FROM PRODUCTOS");
?>
<!-- CSS -->
<style type="text/css">
    .dataTables_filter{
        padding-bottom: 15px;
    }
    table, th, td{
        text-align:center;
    }
</style>
<!-- JavaScript -->
<script type="text/javascript">
	function mensajeConfirmacion(){
		return confirm('¿Está seguro de querer realizar la acción sobre el producto?');
	}
	$(document).ready(function() {
        $("#listaProductos").dataTable( {
    		"bPaginate": false,
    		"bLengthChange": false,
    		"bFilter": true,
    		"bSort": true,
    		"bInfo": false,
    		"bAutoWidth": true,
    		"oLanguage": {
    			"sZeroRecords": "<p>No se han encontrado productos</p>",
    			"sSearch": "Filtrar:"
    		}
       });
       <?php if($error){ ?>
           alert("Error: la clave introducida no es correcta");
       <?php } ?>
    });
</script>
<!-- Content -->
<div class="content">
	<div class="container">
        <?php
            // Control de seguridad
            if(!isset($_SESSION["administrador"])){
        ?>
        <div style="text-align:center;margin-top:25px;">
            <h2>Sección de administración</h2>
            <br/><br/>
            <form action="admin.php" method="POST">
                Contraseña de acceso:
                <input type="password" name="clave" placeholder="contraseña..." />
                <input type="submit" value=" Acceder como administrador " />
            </form>
        </div>
        <?php }else{ ?>
            <!-- Product List -->
            <br/><br/>
            <table id="listaProductos" class="sortable-theme-bootstrap" data-sortable>
    			<thead><tr>
                    <th data-sorted="true" data-sorted-direction="ascending">NOMBRE</th>
    				<th>CATEGORÍA</th>
                    <th>PRECIO</th>
    				<th>MIN. UNIDADES</th>
    				<th>FECHA SUBIDA</th>
                    <th>ACCIONES</th>
    			</tr></thead>
    			<tbody>
    				<?php while($pro = $query->fetch_object()){ ?>
    				<tr>
    					<form action="admin.php" method="POST" onsubmit="return mensajeConfirmacion();">
    						<input type="hidden" name="id" value="<?=$pro->PRO_ID?>" />
    						<td><?=utf8_encode($pro->PRO_NOMBRE)?></td>
    						<td><?=utf8_encode($categorias[$pro->PRO_CATEGORIA])?></td>
    						<td><input type="text" size="6" name="precio" value="<?=$pro->PRO_PRECIO?>" required /> €</td>
    						<td><input type="number" min="1" name="unidades" value="<?=$pro->PRO_MIN_UNIDADES?>" required /></td>
    						<td><?=date('d/m/Y H:i',strtotime($pro->PRO_FECHA))?></td>
    						<td>
                                <input type="submit" name="editar" value="EDITAR" /> &nbsp; &nbsp;
                                <input type="submit" name="eliminar" value="ELIMINAR" />
                            </td>
                        </form>
    				</tr>
    			<?php } ?>
    			</tbody>
    		</table>
            <br/>
            <a href="newproduct.php">Subir un nuevo producto</a>
        <?php } ?>
        <div class="bottom-content">
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<?php
    // Incluimos el pie de página si somos administradores
    if(isset($_SESSION["administrador"])){
       require_once("include/footer.php");
   }else{
	   $mysql->close();
   }
?>
