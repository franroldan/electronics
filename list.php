<?php
	// Cargamos la cabecera de la web
	require_once("include/header.php");

	// Si no hay categoría seleccionada volvemos al home
	$id_categoria = (isset($_GET['category'])) ? (int)$_GET['category'] : null;
	if(is_null($id_categoria) || !isset($categorias[$id_categoria])){
		header("Location: home.php");
        ?><script>location.href="home.php";</script><?php
        exit;
	}
	$categoria = $categorias[$id_categoria];

	// Cargamos el listado de productos de la categoría
	// ordenados por precio de forma ascendente
	$query = $mysql->query("
        SELECT pro.*, (
            SELECT img.IMG_URL
            FROM IMAGENES_PRODUCTOS ip
            JOIN IMAGENES img ON (img.IMG_ID = ip.IP_IMAGEN)
            WHERE ip.IP_PRODUCTO = pro.PRO_ID
			ORDER BY RAND()
			LIMIT 1
        ) AS PRO_URL
        FROM PRODUCTOS pro
        WHERE pro.PRO_CATEGORIA = $id_categoria
		ORDER BY PRO_PRECIO ASC
    ");
?>
<div class="container">
	<div class="content-top-blue">
		<!-- Subtitle -->
		<h2 class="new"><?=utf8_encode($categoria)?></h2>
		<!-- Product list -->
		<div class="pink">
			<?php $numProd = 0;
			while($pro = $query->fetch_object()){ ?>
			<div class="col-md-3 blue">
				<a href="detail.php?product=<?=$pro->PRO_ID?>" >
					<img src="images/<?=$pro->PRO_URL?>" class="img-responsive" alt="Imagen del producto">
				</a>
				<div class="grid_1 simpleCart_shelfItem">
					<a href="detail.php?product=<?=$pro->PRO_ID?>" class="cup item_add">
						<span class="item_price"><?=number_format($pro->PRO_PRECIO,2)?> &euro;</span>
					</a>
				</div>
				<br/>
			</div>
			<?php } ?>
			<div class="clearfix"></div>
		</div>
		<!-- The Best -->
		<?php
			// Incluimos el plan de empresa
			include("include/objetive.html");
	 	?>
	</div>
</div>
<?php
	// Incluimos el pie de página
	require_once("include/footer.php");
?>
