<?php
	// Cargamos la cabecera de la web
	require_once("include/header.php");
?>
<div class="container">
	<div class="four">
		<h1>Ha ocurrido un error</h1>
		<p>
			La página que intentaba visitar no ha podido mostrarse debido a algún fallo.
			Si este error persiste, contacte con el administrador de la web.
		</p>
		<p>Disculmen las molestias que hayamos podido causarle.</p>
		<a href="home.php" class="more go">Volver a la página de inicio</a>
	</div>
</div>
<?php
	// Incluimos el pie de página
	require_once("include/footer.php");
?>
